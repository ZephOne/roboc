# -*-coding:Utf-8 -*

"""This module contains test for Labyrinth Class"""

import unittest
from context import labyrinth, robot


class LabyrinthTest(unittest.TestCase):

    """Test Labyrinth class methodes."""

    def setUp(self):
        """Tests initialisation"""
        self.ascii_labyrinth = b'OOOOOOOOOO\nO O    O O\nO . OO   O\nO O O   XO\nO OOOO O.O\nO O O    U\nO OOOOOO.O\nO O      O\nO O OOOOOO\nO . O    O\nOOOOOOOOOO'.decode()

    def test_create_from_string(self):
        robot_instance = robot.Robot(8, 3, 0)
        obstacles = [(0, 0), (1, 0), (2, 0), (3, 0), (4, 0), (5, 0), (6, 0),
                     (7, 0), (8, 0), (9, 0), (0, 1), (2, 1), (7, 1), (9, 1),
                     (0, 2), (4, 2), (5, 2), (9, 2), (0, 3), (2, 3), (4, 3),
                     (9, 3), (0, 4), (2, 4), (3, 4), (4, 4), (5, 4), (7, 4),
                     (9, 4), (0, 5), (2, 5), (4, 5), (0, 6), (2, 6), (3, 6),
                     (4, 6), (5, 6), (6, 6), (7, 6), (9, 6), (0, 7), (2, 7),
                     (9, 7), (0, 8), (2, 8), (4, 8), (5, 8), (6, 8), (7, 8),
                     (8, 8), (9, 8), (0, 9), (4, 9), (9, 9), (0, 10), (1, 10),
                     (2, 10), (3, 10), (4, 10), (5, 10), (6, 10), (7, 10),
                     (8, 10), (9, 10)]
        doors = [(2, 2), (8, 4), (8, 6), (2, 9)]
        out = (9, 5)
        size = (9, 10)
        labyrinth_create_from_string = labyrinth.Labyrinth.create_from_string(
                self.ascii_labyrinth)
        labyrinth_create_with_init = labyrinth.Labyrinth(robot_instance,
                                                         obstacles,
                                                         doors,
                                                         out,
                                                         size)
        self.assertEqual(labyrinth_create_from_string,
                         labyrinth_create_with_init)
