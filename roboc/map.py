# -*-coding:Utf-8 -*

"""This module contains the Map class."""

from labyrinth import Labyrinth


class Map:

    """Transition object between a file and a labyrinth."""

    def __init__(self, name, string):
        self.name = name
        self.labyrinth = Labyrinth.create_from_string(string)

    def __repr__(self):
        return "<Map {}>".format(self.name)
