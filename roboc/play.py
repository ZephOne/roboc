import sys
import os
import pathlib

from map import Map
from core import (display_resume_game_info,
                  display_game_choice, game_choice_is_valid,
                  make_labyrinth_from_choice, save_game, display_commands,
                  robot_can_go_there)

maps = []
saved_game = False

# On charge les cartes existantes
if sys.platform is 'win32' or 'cigwin':
    user_os = "windows"
else:
    user_os = "unix"

saved_game = pathlib.Path("roboc/saved_game.txt")

for filename in os.listdir("roboc/maps/{}".format(user_os)):
    if filename.endswith(".txt"):
        path = os.path.join("roboc/maps/{}".format(user_os), filename)
        mapname = filename[:-3].lower()
        with open(path, "r") as file:
            content = file.read()
            map = Map(mapname, content)
            maps.append(map)

# On affiche les cartes existantes
print("Existing labyrinths :")
for i, map in enumerate(maps):
    print("  {} - {}".format(i + 1, map.name))

# Si il y a une partie sauvegardée, on l'affiche, à compléter
if saved_game.is_file():
    with open('roboc/saved_game.txt', 'r') as my_file:
        saved_labyrinth = my_file.read()
    saved_game = True
    display_resume_game_info(saved_labyrinth)

display_game_choice(saved_game)

game_choice = input("Your choice?\n> ")

while game_choice_is_valid(game_choice, maps) is False:
    game_choice = input("Your choice?\n> ")

if saved_game is True:
    loaded_labyrinth = make_labyrinth_from_choice(
            game_choice,
            maps,
            saved_labyrinth=saved_labyrinth)
else:
    loaded_labyrinth = make_labyrinth_from_choice(game_choice, maps)

save_game(loaded_labyrinth)

display_commands()

while (loaded_labyrinth.robot.pos_x,
       loaded_labyrinth.robot.pos_y) != loaded_labyrinth.out:
    loaded_labyrinth.draw()
    command = input("\n> ")

    while command[0] not in ['q', 'n', 'e', 's', 'w']:
        print("You didn't give a game command")
        loaded_labyrinth.draw()
        display_commands()
        command = input("\n> ")

    if command[0] is 'q':
        sys.exit()

    direction = command[0]
    nb_step = 1

    if len(command) > 1:
        nb_step = command[1:]
        try:
            nb_step = int(nb_step)
        except ValueError:
            print("You didn't input a number")
            nb_step = 0
            continue

    if robot_can_go_there(loaded_labyrinth, direction, nb_step) is True:
        if direction == 'n':
            loaded_labyrinth.robot.go_to_north(nb_step)
        if direction == 'e':
            loaded_labyrinth.robot.go_to_east(nb_step)
        if direction == 's':
            loaded_labyrinth.robot.go_to_south(nb_step)
        if direction == 'w':
            loaded_labyrinth.robot.go_to_west(nb_step)

        save_game(loaded_labyrinth)

    else:
        print("You can't go there!!!")

print("You win!!!")

os.remove('roboc/saved_game.txt')
