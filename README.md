# My Roboc

## About

My Roboc is a simple game written in python. It is a simple labyrinth game.
The player embodies a robot and must escape the labyrinth to win the game.
There is several maps the player can play.
It is possible for a player to save the game.
