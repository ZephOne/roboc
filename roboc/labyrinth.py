# -*-coding:Utf-8 -*

"""This module contains Labyrinth class."""

from robot import Robot


class Labyrinth:

    """Class modelling a labyrinth."""

    def __init__(self, robot, obstacles, doors, out, size):
        self.robot = robot
        self.obstacles = obstacles
        self.doors = doors
        self.out = out
        self.size = size

    def __eq__(self, labyrinth):
        return (self.robot,
                self.obstacles,
                self.doors,
                self.out,
                self.size) == (
                        labyrinth.robot,
                        labyrinth.obstacles,
                        labyrinth.doors,
                        labyrinth.out,
                        labyrinth.size)

    @classmethod
    def create_from_string(cls, string):
        obstacles = []
        doors = []
        robot = Robot()
        out_x = 0
        out_y = 0
        size_x = 0
        size_y = 0
        for i, line in enumerate(string.split('\n')):
            for j, char in enumerate(line):
                if char == 'O':
                    obstacles.append((j, i))
                if char == '.':
                    doors.append((j, i))
                if char == 'U':
                    out_x = j
                    out_y = i
                if char == 'X':
                    robot.pos_x = j
                    robot.pos_y = i
                size_x = j
                size_y = i

        out = (out_x, out_y)
        size = (size_x, size_y)

        return cls(robot, obstacles, doors, out, size)

    def draw(self, store=False):
        # We draw a matrix of ' '
        draw = ''
        width = self.size[0] + 1
        space_line = width * ' ' + '\n'
        for i in range(self.size[1]):
            draw += space_line

        for obstacle in self.obstacles:
            position = obstacle[1] * (width + 1) + obstacle[0]
            new_draw = draw[:position] + 'O' + draw[position + 1:]
            draw = new_draw

        for door in self.doors:
            position = door[1] * (width + 1) + door[0]
            new_draw = draw[:position] + '.' + draw[position + 1:]
            draw = new_draw

        out_position = self.out[1] * (width + 1) + self.out[0]
        new_draw = draw[:out_position] + 'U' + draw[out_position + 1:]
        draw = new_draw

        robot_position = self.robot.pos_y * (width + 1) + self.robot.pos_x
        new_draw = draw[:robot_position] + 'X' + draw[robot_position + 1:]
        draw = new_draw

        if store is True:
            return draw
        else:
            print(draw)
