# -*-coding:Utf-8 -*

"""This module contains test for Robot Class"""

import unittest
from context import robot


class RobotTest(unittest.TestCase):

    """Test Robot class methodes."""

    def setUp(self):
        """Tests initialisation"""
        self.robot = robot.Robot(5, 10, 3)

    def test_go_to_north(self):
        """Test go_to_north function"""
        self.robot.go_to_north(3)
        self.assertEqual(5, self.robot.pos_x)
        self.assertEqual(7, self.robot.pos_y)
        self.assertEqual(3, self.robot.pos_z)

    def test_go_to_east(self):
        """Test go_to_east function"""
        self.robot.go_to_east(5)
        self.assertEqual(10, self.robot.pos_x)
        self.assertEqual(10, self.robot.pos_y)
        self.assertEqual(3, self.robot.pos_z)

    def test_go_to_south(self):
        """Test go_to_south function"""
        self.robot.go_to_south(6)
        self.assertEqual(5, self.robot.pos_x)
        self.assertEqual(16, self.robot.pos_y)
        self.assertEqual(3, self.robot.pos_z)

    def test_go_to_west(self):
        """Test go_to_west function"""
        self.robot.go_to_west(4)
        self.assertEqual(1, self.robot.pos_x)
        self.assertEqual(10, self.robot.pos_y)
        self.assertEqual(3, self.robot.pos_z)
