# -*-coding:Utf-8 -*

""" This module contains Robot class """


class Robot:
    """Class modelling a Robot"""

    def __init__(self, pos_x=0, pos_y=0, pos_z=0):
        """Robot class constructor.

        An instance of robot is created using its coordinates

        """
        self.pos_x = pos_x
        self.pos_y = pos_y
        self.pos_z = pos_z

    def __repr__(self):
        """Representation of an instance of Robot class"""
        return "Robot in position ({}, {}, {})".format(self.pos_x,
                                                       self.pos_y,
                                                       self.pos_z)

    def __eq__(self, robot):
        return (self.pos_x,
                self.pos_y,
                self.pos_z) == (robot.pos_x,
                                robot.pos_y,
                                robot.pos_z)

    def go_to_north(self, step=1):
        """Make the robot move by 'nb_step' step(s) toward north"""
        self.pos_y -= step

    def go_to_east(self, step=1):
        """Make the robot move by 'nb_step' step(s) toward east"""
        self.pos_x += step

    def go_to_south(self, step=1):
        """Make the robot move by 'nb_step' step(s) toward south"""
        self.pos_y += step

    def go_to_west(self, step=1):
        """Make the robot move by 'nb_step' step(s) toward west"""
        self.pos_x -= step
