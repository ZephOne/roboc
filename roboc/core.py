# -*-coding:Utf-8 -*

from labyrinth import Labyrinth


def display_commands():
    """Print games command to the user"""
    msg = "\tq\tSave and quit"
    msg += "\n\tn<n>\tMove by <n> steps to the north"
    msg += "\n\te<n>\tMove by <n> steps to the east"
    msg += "\n\ts<n>\tMove by <n> steps to the south"
    msg += "\n\to<n>\tMove by <n> steps to the west"
    print(msg)


def display_resume_game_info(saved_labyrinth):
    """Print information concerning the saved game"""
    msg = "Last time you saved this game: \n\n"
    msg += saved_labyrinth
    print(msg)


def display_game_choice(saved_game=False):
    """Print game choices"""
    msg = "What do you want to do ?"
    msg += "\n\t<n>\tChoose the labyrinth #<n>"
    if saved_game is True:
        msg += "\n\ts\tResume saved game"
    print(msg)


def save_game(labyrinth):
    """Save current labyrinth"""
    with open('roboc/saved_game.txt', 'w+') as my_file:
        saved_labyrinth = labyrinth.draw(store=True)
        my_file.write(saved_labyrinth)


def game_choice_is_valid(game_choice, maps):
    if game_choice == "s":
        return True
    elif game_choice.isdecimal():
        map_number = int(game_choice)
        if map_number in range(1, len(maps) + 1):
            return True
    else:
        return False


def make_labyrinth_from_choice(game_choice, maps, **kwargs):
    """Return an instance of Labyrinth"""
    if game_choice == "s":
        return Labyrinth.create_from_string(kwargs['saved_labyrinth'])

    game_choice = int(game_choice)

    return maps[game_choice - 1].labyrinth


def robot_can_go_there(loaded_labyrinth, direction, nb_step):
    """Verify if a robot can go from its position to the
    position given by 'direction' and 'nb_step'"""
    if direction == 'n':
        if loaded_labyrinth.robot.pos_y - nb_step < 0:
            return False
        for step in range(1, nb_step+1):
            if (
                    (loaded_labyrinth.robot.pos_x,
                        loaded_labyrinth.robot.pos_y - step)
                    in loaded_labyrinth.obstacles):
                return False
    if direction == 'e':
        if loaded_labyrinth.robot.pos_x + nb_step > loaded_labyrinth.size[1]:
            return False
        for step in range(1, nb_step+1):
            if(
                    (loaded_labyrinth.robot.pos_x + step,
                        loaded_labyrinth.robot.pos_y)
                    in loaded_labyrinth.obstacles):
                return False
    if direction == 's':
        if loaded_labyrinth.robot.pos_y + nb_step > loaded_labyrinth.size[0]:
            return False
        for step in range(1, nb_step+1):
            if(
                    (loaded_labyrinth.robot.pos_x,
                        loaded_labyrinth.robot.pos_y + step)
                    in loaded_labyrinth.obstacles):
                return False
    if direction == 'w':
        if loaded_labyrinth.robot.pos_x - nb_step < 0:
            return False
        for step in range(1, nb_step+1):
            if(
                    (loaded_labyrinth.robot.pos_x - step,
                        loaded_labyrinth.robot.pos_y)
                    in loaded_labyrinth.obstacles):
                return False

    return True
